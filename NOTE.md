Export specific field in mongodb to file
---

https://stackoverflow.com/questions/6697813/how-use-mongoexport-to-export-only-specific-fields-in-a-sub-document

```
mongoexport -d bookzen -c books -f description --csv -o data/des.csv
```

```
mongoexport -d bookzen -c books --csv -o data/des.csv --fields _id, spider, name, price, url, description, name_unicode
```

Auto reload ipython
---

```
%load_ext autoreload
%autoreload 2

or

The autoreload extension is already loaded. To reload it, use:
%reload_ext autoreload
```


Backup and Restore MongoDB
---

```
mongodump -d bookzen -o backup_path

mongorestore -d bookzen2 backup_path/bookzen

```

Delete document in MongoDB
---

```
https://stackoverflow.com/questions/32949898/deleting-document-in-pymongo-from-id
```

Add field in MongoDB
---

```
https://stackoverflow.com/questions/15666169/python-pymongo-how-to-insert-a-new-field-on-an-existing-document-in-mongo-fro
```

Remove deleted file from git commit
---
```
git ls-files -dz | xargs -0 git rm
```
