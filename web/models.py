from mongoengine import *


connect(db="bookzen", host="mongodb://localhost:27017")


class Books(Document):
    name = StringField()
    name_unidecode = StringField()
    author = StringField()
    description = StringField()
    image_uri = StringField()
    price = StringField()

    raw_description = StringField()
    vni_word_segmentation = StringField()

    url = StringField()
    spider = StringField()
    server = StringField()
    project = StringField()
    date = DateTimeField()

    meta = {'indexes': [
        {'fields': ['$name', "$name_unidecode", "$description"]}]}
