import os
import sys
import logging

from flask import Flask, render_template, redirect, url_for, jsonify
from sklearn.externals import joblib
import pymongo
from gensim.models import Word2Vec

# from models import Books
from forms import SearchForm
# extend path
sys.path.insert(0, "/home/phanhoang/workspace/projects/RS/src")
from fuzzy_search_mongo import (
    search_query_and_ngrams,
    process_keyword_query
)  # noqa


client = pymongo.MongoClient('mongodb://localhost:27017')
db = client["bookzen"]
mongo_col = db["books"]
# instantiate the app
app = Flask(__name__)
app.secret_key = os.environ.get("SECRET_KEY", "aaa")

# set config
# app.config.from_object('web.config.DevelopmentConfig')


logging.basicConfig(
    format='%(asctime)s : %(levelname)s : %(message)s',
    level=logging.INFO
)

model = Word2Vec.load(
    '/home/phanhoang/workspace/projects/RS/src/model/100features_10context_7minwords.vec'  # noqa
)
rs = joblib.load(
    "/home/phanhoang/workspace/projects/RS/src/model/knn_dump.pkl"
)


# TEMPLATE FILTER
@app.template_filter()
def make_capitalize(text):
    return text.capitalize()


@app.template_filter()
def split_lines(text):
    return "\n".join(text.split("."))


@app.template_filter()
def remove_underscore(text):
    return " ".join(text.split("_"))


class MongoConnect(object):

    def __init__(self, mongo_uri, mongo_db, document='books'):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db
        self.document = document
        self.open_connection()

    def open_connection(self):
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]
        self.mongo_col = self.db[self.document]

    def close_connection(self):
        self.client.close()

    def process_item(self):
        for item in self.mongo_col.find():
            yield item

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close_connection()
        return True  # If True: Suppress this exception


@app.route('/ping', methods=['GET'])
def ping_pong():
    return jsonify({
        'call': 'success',
        'message': 'pong!'
    })


@app.route('/', methods=["GET", "POST"])
def index():
    form = SearchForm()
    if form.validate_on_submit():
        keyword = form.search.data
        return redirect(url_for('search', keyword=keyword))
    else:
        return render_template('index.html', form=form)


@app.route('/search/<keyword>', methods=["GET", "POST"])
def search(keyword):

    form = SearchForm()
    if form.validate_on_submit():
        keyword = form.search.data
        return redirect(url_for('search', keyword=keyword))

    keyword = keyword.lower()
    ids = search_query_and_ngrams(keyword)[:24]
    keyword, max_score = process_keyword_query(ids, keyword)
    print(keyword, max_score)
    search_query_and_ngrams(keyword)

    try:
        most_similar = model.wv.most_similar(positive=[keyword.lower()])[:5]
        print("- {}: {}".format(keyword, most_similar))
        most_similar = [word[0] for word in most_similar]
    except KeyError as e:
        most_similar = []
        print(e)

    books = list(mongo_col.find({"ids": {"$in": ids}}))
    return render_template(
        'results.html', form=form, books=books,
        keyword=keyword, most_similar=most_similar
    )


@app.route('/books/<int:book_id>', methods=["GET", "POST"])
def show_post(book_id):

    form = SearchForm()
    if form.validate_on_submit():
        keyword = form.search.data
        return redirect(url_for('search', keyword=keyword))

    main_book = mongo_col.find_one({"ids": int(book_id)})
    # with MongoConnect('mongodb://localhost:27017', 'bookzen') as db:
    #     book = db.mongo_col.find({"ids": int(book_id)})

    vectorizer = rs.transform([main_book["raw_description"]])
    ind_documents = rs.evaluate(vectorizer)[:18]
    ind_documents = [int(i) for i in ind_documents]
    print(ind_documents)
    # print(text)
    # with MongoConnect('mongodb://localhost:27017', 'bookzen') as db:
    #     books = db.mongo_col.find({"ids": {"$in"  : ind_documents}})
    books = list(mongo_col.find({"ids": {"$in": ind_documents}}))
    result = []
    for book in books:
        result.append({
            "name": book["name"],
            "image_uri": book["image_uri"],
            "url": book["url"],
            "spider": book["spider"],
            "price": book.get("price", "Unknown"),
            "ids": book["ids"],
        })
    # import json
    # context = [dict(json.loads(book.to_json())) for book in books]
    return render_template(
        'results.html', book=main_book, books=result, form=form
    )


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
