from flask_wtf import FlaskForm as Form
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired


class SearchForm(Form):
    flash_msg = "Please search something so we can serve you"
    search = StringField("Search book\'s title", validators=[
                         DataRequired(flash_msg)])
    submit = SubmitField()
