import pymongo


class MongoConnect(object):

    def __init__(self, mongo_uri, mongo_db, document='books'):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db
        self.document = document
        self.open_connection()

    def open_connection(self):
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]
        self.mongo_col = self.db[self.document]

    def close_connection(self):
        self.client.close()

    def process_item(self):
        for item in self.mongo_col.find():
            yield item

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close_connection()
        return True  # If True: Suppress this exception
