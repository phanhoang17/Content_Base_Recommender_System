from functools import wraps
import time
import warnings


def deprecated(func):
    '''This is a decorator which can be used to mark functions
    as deprecated. It will result in a warning being emitted
    when the function is used.'''
    def new_func(*args, **kwargs):
        warnings.warn(
            "Call to deprecated function {}.".format(func.__name__),
            category=DeprecationWarning
        )
        print("Call to deprecated function {}.".format(func.__name__))
        return func(*args, **kwargs)
    new_func.__name__ = func.__name__
    new_func.__doc__ = func.__doc__
    new_func.__dict__.update(func.__dict__)
    return new_func


def timer(func):

    @wraps(func)
    def wrapper(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()
        print("{} tooks {} sec".format(func.__name__, end - start))
        return result

    return wrapper


def logged_timer(time_format):
    def decorator(func):
        def decorated_func(*args, **kwargs):
            print("- Running '%s' on %s " % (
                func.__name__,
                time.strftime(time_format)
            ))
            start_time = time.time()
            result = func(*args, **kwargs)
            end_time = time.time()
            print("- Finished '%s', execution time = %0.3fs " % (
                func.__name__,
                end_time - start_time
            ))
            return result
        # decorated_func.__name__ = func.__name__
        return decorated_func
    return decorator


def logged(time_format="%b %d %Y - %H:%M:%S", name_prefix=""):
    def decorator(func):
        if hasattr(func, '_logged_decorator') and func._logged_decorator:
            return func

        @wraps(func)
        def decorated_func(*args, **kwargs):
            start_time = time.time()
            print("- Running '%s' on %s " % (
                name_prefix + func.__name__,
                time.strftime(time_format)
            ))
            result = func(*args, **kwargs)
            end_time = time.time()
            print("- Finished '%s', execution time = %0.3fs " % (
                name_prefix + func.__name__,
                end_time - start_time
            ))

            return result
        decorated_func._logged_decorator = True
        return decorated_func
    return decorator


def log_method_calls(time_format="%b %d %Y - %H:%M:%S"):
    def decorator(cls):
        for o in dir(cls):
            if o.startswith('__'):
                continue
            a = getattr(cls, o)
            if hasattr(a, '__call__'):
                decorated_a = logged(time_format, cls.__name__ + ".")(a)
                setattr(cls, o, decorated_a)
        return cls
    return decorator


def log_method_calls2(time_format="%b %d %Y - %H:%M:%S"):
    def decorator(klass):
        for attr in dir(klass):
            if attr.startswith('__'):
                continue
            attr_ = getattr(klass, attr)
            decorated_a = logged_timer(time_format)(attr_)
            setattr(klass, attr, decorated_a)
        return klass
    return decorator
