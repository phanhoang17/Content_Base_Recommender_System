from __future__ import print_function

from sklearn.cluster import (
    KMeans,
    MiniBatchKMeans,
)
from sklearn.decomposition import TruncatedSVD
from sklearn.externals import joblib
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import make_pipeline, Pipeline
from sklearn.preprocessing import Normalizer
from sklearn.metrics import pairwise_distances


import logging
from optparse import OptionParser
import sys
from time import time

from dbconnection import MongoConnect

# Display progress logs on stdout
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s')

# parse commandline arguments
op = OptionParser()
op.add_option("--lsa",
              dest="n_components", type="int", default=256,
              help="Preprocess documents with latent semantic analysis.")
op.add_option("--no-minibatch",
              action="store_false", dest="minibatch", default=True,
              help="Use ordinary k-means algorithm (in batch mode).")
op.add_option("--no-idf",
              action="store_false", dest="use_idf", default=True,
              help="Disable Inverse Document Frequency feature weighting.")
op.add_option("--use-hashing",
              action="store_true", default=False,  # False
              help="Use a hashing feature vectorizer")
op.add_option("--n-features", type=int, default=10000,
              help="Maximum number of features (dimensions)"
                   " to extract from text.")
op.add_option("--verbose",
              action="store_true", dest="verbose", default=False,
              help="Print progress reports inside k-means algorithm.")


def is_interactive():
    return not hasattr(sys.modules['__main__'], '__file__')


# work-around for Jupyter notebook and IPython console
argv = [] if is_interactive() else sys.argv[1:]
(opts, args) = op.parse_args(argv)
if len(args) > 0:
    op.error("this script takes no arguments.")
    sys.exit(1)


def _get_dataset():
    bookzen = MongoConnect('mongodb://localhost:27017', 'bookzen')
    books = bookzen.process_item()

    def process_field(field="raw_description"):
        for book in books:
            yield book[field]

    dataset = process_field()
    bookzen.close_connection()
    return dataset


dataset = _get_dataset()


# list vietnamese's stopwords
stop_words = []
with open('utils/stopword.txt') as f:
    for line in f:
        stop_words.append(line.strip())


print("Extracting features from the training dataset using a sparse vectorizer")
t0 = time()
if opts.use_hashing:
    if opts.use_idf:
        # Perform an IDF normalization on the output of HashingVectorizer
        hasher = HashingVectorizer(n_features=None,
                                   stop_words=stop_words, alternate_sign=False,
                                   norm=None, binary=False)
        vectorizer = make_pipeline(hasher, TfidfTransformer())
    else:
        vectorizer = HashingVectorizer(n_features=None,
                                       stop_words=stop_words,
                                       alternate_sign=False, norm='l2',
                                       binary=False)
else:
    vectorizer = TfidfVectorizer(max_df=0.5, max_features=None,
                                 min_df=2, stop_words=stop_words,
                                 use_idf=opts.use_idf)
del stop_words
X = vectorizer.fit_transform(dataset)
print(X.shape)  # (xxxxx, 10000)
del dataset

print("done in %fs" % (time() - t0))
print("n_samples: %d, n_features: %d" % X.shape)
print()

opts.n_components = 256  # recommended: 100
if opts.n_components:
    print("Performing dimensionality reduction using LSA")
    t0 = time()
    # Vectorizer results are normalized, which makes KMeans behave as
    # spherical k-means for better results. Since LSA/SVD results are
    # not normalized, we have to redo the normalization.
    svd = TruncatedSVD(opts.n_components)
    normalizer = Normalizer(copy=False)
    lsa = make_pipeline(svd, normalizer)

    X = lsa.fit_transform(X)
    print('X shape: ', X.shape)  # (-1, 256)

    print("done in %fs" % (time() - t0))

    explained_variance = svd.explained_variance_ratio_.sum()
    print("Explained variance of the SVD step: {}%".format(
        int(explained_variance * 100)))

    # (number of documents, n_components)
    # print(X.shape, type(X))
    # print(X)
    # import numpy as np
    # X_new = np.dot(X, X.T)
    # print(X_new.shape, type(X_new))
    # print(X_new)

    ########
    # implement in scipy
    # from scipy.linalg import svd
    # from scipy.sparse.linalg import svds
    # U, S, V = svds(X)
    # print(S, type(S), S.shape)


# #############################################################################
# Do the actual clustering

n_clusters = 100
if opts.minibatch:
    print("using minibatch kmean")
    km = MiniBatchKMeans(
        n_clusters=n_clusters,
        init='k-means++',
        n_init=1,
        init_size=1000,
        batch_size=1000,
        verbose=opts.verbose
    )
else:
    km = KMeans(
        n_clusters=n_clusters,
        init='k-means++',
        max_iter=100,
        n_init=1,
        verbose=opts.verbose
    )

# using Mean-Shift clustering
# The following bandwidth can be automatically detected using
# bandwidth = estimate_bandwidth(X, quantile=0.2, n_samples=10000)

# km = MeanShift(bandwidth=bandwidth, bin_seeding=True)

#############################


print("Clustering sparse data with %s" % km)
t0 = time()
if opts.minibatch:
    km.partial_fit(X)
else:
    km.fit(X)
print("done in %0.3fs" % (time() - t0))

print("saving model")

file_dump = 'data/kmean_dump.pkl'
joblib.dump(km, file_dump)
print("finish save model!")


# file_dump = 'km_dump.pkl'
# km = joblib.load(file_dump)
# print('X[0] =======>', X[0])
# for i in random.sample(range(len(dataset)), 20):
#     print('dataset =======>', dataset[i], '<========')
#     print(km.predict(X[i]))

# print("finished")
print(vectorizer.vocabulary_)
print(len(vectorizer.vocabulary_))  # 10000
if not opts.use_hashing:
    print("Top terms per cluster:")
    print(km.cluster_centers_.shape)  # (100, 256)
    if opts.n_components:
        original_space_centroids = svd.inverse_transform(km.cluster_centers_)
        print("original_space_centroids: ", original_space_centroids)
        order_centroids = original_space_centroids.argsort()[:, ::-1]
    else:
        order_centroids = km.cluster_centers_.argsort()[:, ::-1]

    print(order_centroids.shape)  # (100, 10000)
    terms = vectorizer.get_feature_names()
    # print("terms", terms)
    print(len(terms))
    for i in range(n_clusters):
        print("Cluster %d:" % i, end='')
        for ind in order_centroids[i, :25]:
            print(' %s' % terms[ind], end='')
        print()
