from __future__ import print_function

from sklearn.cluster import (
    KMeans,
    MiniBatchKMeans,
    # MeanShift,
    # estimate_bandwidth
)
from sklearn.decomposition import TruncatedSVD
from sklearn.externals import joblib
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import make_pipeline, Pipeline
from sklearn.preprocessing import Normalizer
from sklearn.metrics import pairwise_distances
# from sklearn import metrics
# https://github.com/chrisjmccormick/LSA_Classification/blob/master/runClassification_LSA.py

import logging
from optparse import OptionParser
import sys
from time import time

from dbconnection import MongoConnect

from recommend import build_nearest_neighbors

# Display progress logs on stdout
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s')

# parse commandline arguments
op = OptionParser()
op.add_option("--lsa",
              dest="n_components", type="int", default=256,
              help="Preprocess documents with latent semantic analysis.")
op.add_option("--no-minibatch",
              action="store_false", dest="minibatch", default=True,
              help="Use ordinary k-means algorithm (in batch mode).")
op.add_option("--no-idf",
              action="store_false", dest="use_idf", default=True,
              help="Disable Inverse Document Frequency feature weighting.")
op.add_option("--use-hashing",
              action="store_true", default=False,  # False
              help="Use a hashing feature vectorizer")
op.add_option("--n-features", type=int, default=10000,
              help="Maximum number of features (dimensions)"
                   " to extract from text.")
op.add_option("--verbose",
              action="store_true", dest="verbose", default=False,
              help="Print progress reports inside k-means algorithm.")


def is_interactive():
    return not hasattr(sys.modules['__main__'], '__file__')


# work-around for Jupyter notebook and IPython console
argv = [] if is_interactive() else sys.argv[1:]
(opts, args) = op.parse_args(argv)
if len(args) > 0:
    op.error("this script takes no arguments.")
    sys.exit(1)


bookzen = MongoConnect('mongodb://localhost:27017', 'bookzen')
books = bookzen.process_item()


def process_field(field="raw_description"):
    for book in books:
        yield book[field]


dataset = process_field()
# for book in books:
#     dataset.append(book["raw_description"])


bookzen.close_connection()


# list vietnamese's stopwords
stop_words = []
with open('utils/stopword.txt') as f:
    for line in f:
        stop_words.append(line.strip())


print("Extracting features from the training dataset using a sparse vectorizer")
t0 = time()
if opts.use_hashing:
    if opts.use_idf:
        # Perform an IDF normalization on the output of HashingVectorizer
        hasher = HashingVectorizer(n_features=opts.n_features,
                                   stop_words=stop_words, alternate_sign=False,
                                   norm=None, binary=False)
        vectorizer = make_pipeline(hasher, TfidfTransformer())
    else:
        vectorizer = HashingVectorizer(n_features=opts.n_features,
                                       stop_words=stop_words,
                                       alternate_sign=False, norm='l2',
                                       binary=False)
else:
    # TfidfVectorizer = (CountVectorizer -> TfidfTransformer)
    vectorizer = TfidfVectorizer(max_df=0.5, max_features=opts.n_features,
                                 min_df=2, stop_words=stop_words,
                                 use_idf=opts.use_idf)
del stop_words
X = vectorizer.fit_transform(dataset)
# print(X.shape)
# del dataset

print("done in %fs" % (time() - t0))
print("n_samples: %d, n_features: %d" % X.shape)
print()

opts.n_components = 256  # recommended by sklearn: 100
if opts.n_components:
    print("Performing dimensionality reduction using LSA")
    t0 = time()
    svd = TruncatedSVD(opts.n_components)
    normalizer = Normalizer(copy=False)
    lsa = make_pipeline(svd, normalizer)

    X = lsa.fit_transform(X)
    print(X)
    print('shape of X: ', X.shape)
    print('type of X: ', type(X))

    print("saving X matrix")

    file_dump = 'data/X_matrix.pkl'
    joblib.dump(X, file_dump)
    print("finish save X matrix!")

    # print("saving X_ids zip")
    # file_dump = 'data/X_ids.pkl'
    # X_ids = (X, list(process_field("_id")))
    # joblib.dump(X_ids, file_dump)
    # print("finish save X_ids zip!")

    print("done in %fs" % (time() - t0))

    explained_variance = svd.explained_variance_ratio_.sum()
    print("Explained variance of the SVD step: {}%".format(
        int(explained_variance * 100)))

    rs_model = build_nearest_neighbors(X)  # 20 topics

    print("saving model")

    file_dump = 'data/knn_dump.pkl'
    joblib.dump(rs_model, file_dump)
    # del km
    print("finish save model!")

del dataset
